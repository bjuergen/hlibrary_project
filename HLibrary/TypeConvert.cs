using System;

namespace HLibrary
{
    public static class TypeConvert 
    {
        /// <summary>
        /// Convert 
        /// </summary>
        /// <param name="string">The string you want to convert to Int32</param>
        /// <returns>Returns a Int32 value</returns>

        public static Int32 ToInt32(this string s)
        {
            Int32 ret;
            bool b;
            
            b = Int32.TryParse(s,out ret);
            if (!b)
                throw new Exception($"Cannot convert '{s}' to Int32");
            return ret;
        }



        
    }
}