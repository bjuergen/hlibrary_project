﻿using System;

namespace HLibrary
{
    public class TimeConvert
    {
        /// <summary>
        /// Convert Unix time value to a DateTime object.
        /// </summary>
        /// <param name="unixtime">The Unix time stamp you want to convert to DateTime.</param>
        /// <returns>Returns a DateTime object that represents value of the Unix time.</returns>
        public static DateTime UnixTimeToDateTime(long unixtime)
        {
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixtime).ToLocalTime();
            return dtDateTime;
        }

        /// <summary>
        /// Convert a DateTime to a unix timestamp
        /// </summary>
        /// <param name="MyDateTime">The DateTime object to convert into a Unix Time</param>
        /// <returns>Returns a Unix timestamp.</returns>
        public static long DateTimeToUnix(DateTime MyDateTime)
        {
            TimeSpan timeSpan = MyDateTime - new DateTime(1970, 1, 1, 0, 0, 0);
            return (long)timeSpan.TotalSeconds;
        }


        /// <summary>
        /// Generate a timestamp for logfiles
        /// </summary>
        /// <returns>Returns a timestamp string</returns>
        public static string Timestamp()
        {
            return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
        }


    }
}
