using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;


namespace HLibraryTest
{
    [TestClass]
    public class UnitTest2
    {
        [TestMethod]
        public void CircularBuffer_GetEnumeratorConstructorCapacity_ReturnsEmptyCollection()
        {
            
            var buffer = new HLibrary.CircularBuffer<string>(5);
            // Assert.IsNull(buffer.ToArray());
            // Assert.AreEqual(new[] { 0,0,0,0,0 }, buffer.ToArray());
            // before: CollectionAssert.IsEmpty(buffer.ToArray());
        }

     [TestMethod]
        public void CircularBuffer_ConstructorSizeIndexAccess_CorrectContent()
        {
            var buffer = new HLibrary.CircularBuffer<int>(5, new[] { 0, 1, 2, 3 });
            Assert.AreEqual(5, buffer.Capacity);
            Assert.AreEqual(4, buffer.Size);
            // before: Assert.That(buffer.Capacity, Is.EqualTo(5));
            // before: Assert.That(buffer.Size, Is.EqualTo(4));

            for (int i = 0; i < 4; i++)
            {
                Assert.AreEqual(i, buffer[i]);
                // before: Assert.That(buffer[i], Is.EqualTo(i));
            } 
        }
        
        [TestMethod]
        [ExpectedException(typeof(ArgumentException),
        "Too many items to fit circular buffer.")]
        public void CircularBuffer_Constructor_ExceptionWhenSourceIsLargerThanCapacity()
        {
            var buffer = new HLibrary.CircularBuffer<int>(3, new[] { 0, 1, 2, 3 });
            // before: Assert.That(() => new CircularBuffer<int>(3, new[] { 0, 1, 2, 3 }),
            // before:      Throws.Exception.TypeOf<ArgumentException>());
        }
        
        [TestMethod]
        public void CircularBuffer_GetEnumeratorConstructorDefinedArray_CorrectContent()
        {
            var buffer = new HLibrary.CircularBuffer<int>(5, new[] { 0, 1, 2, 3 });

            int x = 0;
            foreach (var item in buffer)
            {
                Assert.AreEqual(x, item);
                // before: Assert.That(item, Is.EqualTo(x));
                x++;
            }
        }
        
        [TestMethod]
        public void CircularBuffer_PushBack_CorrectContent()
        {
            var buffer = new HLibrary.CircularBuffer<int>(5);

            for (int i = 0; i < 5; i++)
            {
                buffer.PushBack(i);
            }
            
            Assert.AreEqual(0, buffer.Front());
            // before: Assert.That(buffer.Front(), Is.EqualTo(0));
            for (int i = 0; i < 5; i++)
            {
                Assert.AreEqual(i,buffer[i]);
                // before: Assert.That(buffer[i], Is.EqualTo(i));
            }
        }
        
        [TestMethod]
        public void CircularBuffer_PushBackOverflowingBuffer_CorrectContent()
        {
            var buffer = new HLibrary.CircularBuffer<int>(5);

            for (int i = 0; i < 10; i++)
            {
                buffer.PushBack(i);
            }

            // before: Assert.That(buffer.ToArray(), Is.EqualTo(new[] { 5, 6, 7, 8, 9 }));
            CollectionAssert.AreEqual(new[] { 5, 6, 7, 8, 9 }, buffer.ToArray());
            
        } 

        [TestMethod]
        public void CircularBuffer_GetEnumeratorOverflowedArray_CorrectContent()
        {
            var buffer = new HLibrary.CircularBuffer<int>(5);

            for (int i = 0; i < 10; i++)
            {
                buffer.PushBack(i);
            }

            // buffer should have [5,6,7,8,9]
            int x = 5;
            foreach (var item in buffer)
            {
                // before: Assert.That(item, Is.EqualTo(x));
                Assert.AreEqual(x,item);
                x++;
            }
        }
        
        [TestMethod]
        public void CircularBuffer_ToArrayConstructorDefinedArray_CorrectContent()
        {
            var buffer = new HLibrary.CircularBuffer<int>(5, new[] { 0, 1, 2, 3 });

            //before: Assert.That(buffer.ToArray(), Is.EqualTo(new[] { 0, 1, 2, 3 }));
            CollectionAssert.AreEqual(new[] {  0, 1, 2, 3 }, buffer.ToArray());
        }

        [TestMethod]
        public void CircularBuffer_ToArrayOverflowedBuffer_CorrectContent()
        {
            var buffer = new HLibrary.CircularBuffer<int>(5);

            for (int i = 0; i < 10; i++)
            {
                buffer.PushBack(i);
            }

            // before: Assert.That(buffer.ToArray(), Is.EqualTo(new[] { 5, 6, 7, 8, 9 }));
            CollectionAssert.AreEqual(new[] { 5, 6, 7, 8, 9 }, buffer.ToArray());
        }
/*
        [TestMethod]
        public void CircularBuffer_PushFront_CorrectContent()
        {
            var buffer = new CircularBuffer<int>(5);

            for (int i = 0; i < 5; i++)
            {
                buffer.PushFront(i);
            }

            Assert.That(buffer.ToArray(), Is.EqualTo(new[] { 4, 3, 2, 1, 0 }));
        }

        [TestMethod]
        public void CircularBuffer_PushFrontAndOverflow_CorrectContent()
        {
            var buffer = new CircularBuffer<int>(5);

            for (int i = 0; i < 10; i++)
            {
                buffer.PushFront(i);
            }

            Assert.That(buffer.ToArray(), Is.EqualTo(new[] { 9, 8, 7, 6, 5 }));
        }

        [TestMethod]
        public void CircularBuffer_Front_CorrectItem()
        {
            var buffer = new CircularBuffer<int>(5, new[] { 0, 1, 2, 3, 4 });

            Assert.That(buffer.Front(), Is.EqualTo(0));
        }

        [TestMethod]
        public void CircularBuffer_Back_CorrectItem()
        {
            var buffer = new CircularBuffer<int>(5, new[] { 0, 1, 2, 3, 4 });
            Assert.That(buffer.Back(), Is.EqualTo(4));
        }

        [TestMethod]
        public void CircularBuffer_BackOfBufferOverflowByOne_CorrectItem()
        {
            var buffer = new CircularBuffer<int>(5, new[] { 0, 1, 2, 3, 4 });
            buffer.PushBack(42);
            Assert.That(buffer.ToArray(), Is.EqualTo(new[] { 1, 2, 3, 4, 42 }));
            Assert.That(buffer.Back(), Is.EqualTo(42));
        }

        [TestMethod]
        public void CircularBuffer_Front_EmptyBufferThrowsException()
        {
            var buffer = new CircularBuffer<int>(5);

            Assert.That(() => buffer.Front(),
                         Throws.Exception.TypeOf<InvalidOperationException>().
                         With.Property("Message").Contains("empty buffer"));
        }

        [TestMethod]
        public void CircularBuffer_Back_EmptyBufferThrowsException()
        {
            var buffer = new CircularBuffer<int>(5);
            Assert.That(() => buffer.Back(),
                         Throws.Exception.TypeOf<InvalidOperationException>().
                         With.Property("Message").Contains("empty buffer"));
        }

        [TestMethod]
        public void CircularBuffer_PopBack_RemovesBackElement()
        {
            var buffer = new CircularBuffer<int>(5, new[] { 0, 1, 2, 3, 4 });

            Assert.That(buffer.Size, Is.EqualTo(5));

            buffer.PopBack();

            Assert.That(buffer.Size, Is.EqualTo(4));
            Assert.That(buffer.ToArray(), Is.EqualTo(new[] { 0, 1, 2, 3 }));
        }

        [TestMethod]
        public void CircularBuffer_PopBackInOverflowBuffer_RemovesBackElement()
        {
            var buffer = new CircularBuffer<int>(5, new[] { 0, 1, 2, 3, 4 });
            buffer.PushBack(5);

            Assert.That(buffer.Size, Is.EqualTo(5));
            Assert.That(buffer.ToArray(), Is.EqualTo(new[] { 1, 2, 3, 4, 5 }));

            buffer.PopBack();

            Assert.That(buffer.Size, Is.EqualTo(4));
            Assert.That(buffer.ToArray(), Is.EqualTo(new[] { 1, 2, 3, 4 }));
        }

        [TestMethod]
        public void CircularBuffer_PopFront_RemovesBackElement()
        {
            var buffer = new CircularBuffer<int>(5, new[] { 0, 1, 2, 3, 4 });

            Assert.That(buffer.Size, Is.EqualTo(5));

            buffer.PopFront();

            Assert.That(buffer.Size, Is.EqualTo(4));
            Assert.That(buffer.ToArray(), Is.EqualTo(new[] { 1, 2, 3, 4 }));
        }

        [TestMethod]
        public void CircularBuffer_PopFrontInOverflowBuffer_RemovesBackElement()
        {
            var buffer = new CircularBuffer<int>(5, new[] { 0, 1, 2, 3, 4 });
            buffer.PushFront(5);

            Assert.That(buffer.Size, Is.EqualTo(5));
            Assert.That(buffer.ToArray(), Is.EqualTo(new[] { 5, 0, 1, 2, 3 }));

            buffer.PopFront();

            Assert.That(buffer.Size, Is.EqualTo(4));
            Assert.That(buffer.ToArray(), Is.EqualTo(new[] { 0, 1, 2, 3 }));
        }

        [TestMethod]
        public void CircularBuffer_SetIndex_ReplacesElement()
        {
            var buffer = new CircularBuffer<int>(5, new[] { 0, 1, 2, 3, 4 });

            buffer[1] = 10;
            buffer[3] = 30;

            Assert.That(buffer.ToArray(), Is.EqualTo(new[] { 0, 10, 2, 30, 4 }));
        }

        [TestMethod]
        public void CircularBuffer_WithDifferentSizeAndCapacity_BackReturnsLastArrayPosition()
        {
            // test to confirm this issue does not happen anymore:
            // https://github.com/joaoportela/CircularBuffer-CSharp/issues/2

            var buffer = new CircularBuffer<int>(5, new[] { 0, 1, 2, 3, 4 });

            buffer.PopFront(); // (make size and capacity different)

            Assert.That(buffer.Back(), Is.EqualTo(4));
        } */

    }
}