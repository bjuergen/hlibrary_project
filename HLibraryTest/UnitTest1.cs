using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;


namespace HLibraryTest
{
    [TestClass]
    public class UnitTest1
    {
        

        [TestMethod]
        public void TestMethod1()
        {
            
            // Unix -> DateTime                    
            //
            // Test for 1st of January, 1970 00:00:00 UTC
            // DateTime t1 = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            // Assert.AreEqual(t1, HLibrary.TimeConvert.UnixTimeToDateTime(0));
           

            // DateTime -> Unix (Min-Max Value)
            //
            // Test for 1st of January, 1970 00:00:00 UTC
            DateTime dt1 = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            Assert.AreEqual(0, HLibrary.TimeConvert.DateTimeToUnix(dt1));

            // Test for 19 January, 2038 03:14:07 UTC
            DateTime dt2 = new DateTime(2038, 1, 19, 3, 14, 7, 0, System.DateTimeKind.Utc);
            Assert.AreEqual(2147483647, HLibrary.TimeConvert.DateTimeToUnix(dt2));

        }

        [TestMethod]
        public void TestMethod2()
        {
            // Min-Max Value
            Assert.AreEqual(2147483647, HLibrary.TypeConvert.ToInt32("2147483647"));
            Assert.AreEqual(-2147483648, HLibrary.TypeConvert.ToInt32("-2147483648"));

            // Zero
            Assert.AreEqual(0, HLibrary.TypeConvert.ToInt32("0"));
            Assert.AreEqual(0, HLibrary.TypeConvert.ToInt32("0000"));
           
            // All kind of invalid values.
            Assert.ThrowsException<Exception>(() => HLibrary.TypeConvert.ToInt32("2147483647.0"));
            Assert.ThrowsException<Exception>(() => HLibrary.TypeConvert.ToInt32("-2147483648.0"));
            Assert.ThrowsException<Exception>(() => HLibrary.TypeConvert.ToInt32("0.0"));
            Assert.ThrowsException<Exception>(() => HLibrary.TypeConvert.ToInt32("FFFF"));
        }
    }
}
